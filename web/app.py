from flask import Flask, render_template, request, send_from_directory

app = Flask(__name__)

@app.route("/<path:path>")
def serve(path):
    
    if "//" in path or "~" in path or ".." in path:
        return render_template("403.html")
    elif send_from_directory('pages', path):
        return send_from_directory('pages', path)
    else:
        return render_template("404.html")
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

